package com.zekat.Application.service;

import com.zekat.Application.model.CommonWord;
import com.zekat.Application.repository.CommonWordRepository;
import com.zekat.Application.repository.SentenceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Long.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SentenceServiceTest {
    @Mock
    private SentenceRepository sentenceRepository;

    @Mock
    private CommonWordRepository commonWordRepository;

    @InjectMocks
    private SentenceService sentenceService = new SentenceService();

    @Test
    public void getCommonWordsSuccess() {
        List<CommonWord> response = Arrays.asList(new CommonWord(valueOf(133), 0, "fr", "test", new ArrayList()));

        Mockito.when(commonWordRepository.findByLanguageAndPositionGreaterThanEqualAndPositionLessThan("fr", 0, 1))
                .thenReturn(response);

        List<CommonWord> result = sentenceService.getCommonWords("fr", 0, 1);

        assertEquals(result.size(), 1);
        assertEquals(result.get(0).getId(), valueOf(133));
    }
}
