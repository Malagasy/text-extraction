package com.zekat.Application.service;

import com.zekat.Application.service.TranslationService.ITranslationService;

import java.util.List;
import java.util.stream.Collectors;

public class LocalTranslationService implements ITranslationService {
    public List<String> translate(String fromLanguage, String toLanguage, List<String> texts) {
        return texts.stream().map(text -> "[" + fromLanguage + "->" + toLanguage + "] " + text)
                .collect(Collectors.toList());
    }
}
