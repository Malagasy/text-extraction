package com.zekat.Application.configuration;

import com.zekat.Application.service.LocalTranslationService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TranslationServiceConfig {

    @Bean(name = "translationService")
    public LocalTranslationService localTranslationService() {
        return new LocalTranslationService();
    }
}
