package com.zekat.Application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.zekat.Application.configuration.TranslationServiceConfig;
import com.zekat.Application.controller.UserController.PostCreateUserParam;
import com.zekat.Application.model.User.UserEntity;
import com.zekat.Application.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(TranslationServiceConfig.class)
@AutoConfigureMockMvc
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserControllerIntegrationTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() throws IOException {
        URL resource = getClass().getClassLoader().getResource("database/users.json");
        File file = new File(resource.getFile());
        List<Map> users = JsonPath.parse(file).read("$", List.class);

        List<UserEntity> userEntities = users.stream()
                .map(user -> new UserEntity((String) user.get("email"), (String) user.get("password"),
                        (String) user.get("username"))).collect(Collectors.toList());

        userRepository.saveAll(userEntities);
    }

    @Test
    public void createUserSuccess() throws Exception {

        PostCreateUserParam bodyRequest = new PostCreateUserParam("my@email.com", "myEmail", "pwd91");
        ObjectMapper mapper = new ObjectMapper();
        String jsonBodyRequest = mapper.writeValueAsString(bodyRequest);

        MvcResult result = mockMvc.perform(post("/user").contentType("application/json").content(jsonBodyRequest))
                .andExpect(status().isCreated()).andReturn();
    }
}
