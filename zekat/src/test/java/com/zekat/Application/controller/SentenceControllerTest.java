package com.zekat.Application.controller;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.zekat.Application.model.CommonWord;
import com.zekat.Application.model.GapFillResponse.GapFillResponse;
import com.zekat.Application.model.Sentence;
import com.zekat.Application.service.SentenceService;
import com.zekat.Application.service.TranslationService.ITranslationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SentenceController.class)
public class SentenceControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SentenceService sentenceService;

    @MockBean
    private ITranslationService translationService;

    @Test
    public void allFailure() throws Exception {
        MvcResult result =
                mockMvc.perform(get("/sentences/en").contentType("application/json").param("targetLanguage", "en"))
                        .andReturn();

        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void allSuccess() throws Exception {
        CommonWord commonWordStatic = new CommonWord(11L, 0, "fr", "amour", null);
        Sentence sentenceStatic = new Sentence("123", "awesome text", "fr");

        Mockito.when(sentenceService.getCommonWords(anyString(), anyInt(), anyInt()))
                .thenReturn(Collections.singletonList(commonWordStatic));

        Mockito.when(sentenceService.getSentencesFromCommonWords(anyList()))
                .thenReturn(Collections.singletonList(sentenceStatic));

        Mockito.when(translationService.translate(anyString(), anyString(), anyList()))
                .thenReturn(Collections.singletonList("any thing"));

        Mockito.when(sentenceService.generateGapFill(anyList(), anyList(), anyList())).thenReturn(Collections
                .singletonList(new GapFillResponse(sentenceStatic, "translated text", commonWordStatic, null)));

        MvcResult result = mockMvc.perform(
                get("/sentences/en").contentType("application/json").param("targetLanguage", "fr")
                        .param("startPosition", "0")).andExpect(status().isOk()).andReturn();

        DocumentContext jsonResponse = JsonPath.parse(result.getResponse().getContentAsString());

        assertEquals(jsonResponse.read("$[0].translatedText"), "translated text");
        assertEquals(jsonResponse.read("$[0].correctAnswer"), "amour");
        assertEquals((Integer) jsonResponse.read("$[0].correctAnswerId"), 11);
        assertEquals(jsonResponse.read("$[0].gapFillTextId"), "123");
        assertNull(jsonResponse.read("$[0].choices"));
    }

}
