package com.zekat.Application.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.Getter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "common_words")
@Getter
@TypeDef(name = "list-array", typeClass = ListArrayType.class)
public class CommonWord {
    public CommonWord() {
    }

    public CommonWord(Long id, int position, String language, String word, List<String> sentenceIds) {
        this.id = id;
        this.position = position;
        this.language = language;
        this.word = word;
        this.sentenceIds = sentenceIds;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private int position;
    private String language;
    private String word;

    @Type(type = "list-array")
    @Column(
            name = "sentence_ids",
            columnDefinition = "varchar(50) ARRAY"
    )
    private List<String> sentenceIds;



}
