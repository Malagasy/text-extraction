package com.zekat.Application.model.GapFillResponse;

import com.zekat.Application.model.CommonWord;
import com.zekat.Application.model.Sentence;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class GapFillResponse {

    private String gapFillText;
    private String translatedText;
    private String correctAnswer;
    private String gapFillTextId;
    private Long correctAnswerId;
    private List<String> choices;

    public GapFillResponse(Sentence sentence, String translatedText, CommonWord commonWord, List<String> choices) {
        String originalText = sentence.getText();
        String correctAnswer = commonWord.getWord();

        this.gapFillText = String.join(" ",
                Arrays.stream(originalText.split(" ")).map(word -> correctAnswer.equalsIgnoreCase(word) ? "__" : word)
                        .collect(Collectors.toList()));
        this.translatedText = translatedText;
        this.correctAnswer = correctAnswer;
        this.gapFillTextId = sentence.getSentenceId();
        this.correctAnswerId = commonWord.getId();
        this.choices = choices;
    }
}
