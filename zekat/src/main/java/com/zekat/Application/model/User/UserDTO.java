package com.zekat.Application.model.User;

import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

@Getter
@Setter
public class UserDTO {
    private int id;
    private String email;
    private String username;
    private String token;

    static public UserDTO fromEntity(UserEntity userEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(userEntity, UserDTO.class);
    }
}
