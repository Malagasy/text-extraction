package com.zekat.Application.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sentences")
@Setter
@Getter
public class Sentence {

    @Id
    @Column(name = "sentence_id")
    private String sentenceId;

    private String text;

    public Sentence() {
    }

    private String language;

    public Sentence(String sentenceId, String text, String language) {
        this.sentenceId = sentenceId;
        this.text = text;
        this.language = language;
    }
}
