package com.zekat.Application.controller.UserController;

import lombok.Getter;

@Getter
public class PostCreateUserParam {
    private String email;
    private String username;
    private String password;

    public PostCreateUserParam(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }
}
