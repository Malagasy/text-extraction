package com.zekat.Application.controller;

import com.zekat.Application.model.CommonWord;
import com.zekat.Application.model.GapFillResponse.GapFillResponse;
import com.zekat.Application.model.Sentence;
import com.zekat.Application.service.SentenceService;
import com.zekat.Application.service.TranslationService.ITranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SentenceController {

    @Autowired
    private ITranslationService translationService;

    @Autowired
    private SentenceService sentenceService;

    @GetMapping("/sentences/{lang}")
    public List<GapFillResponse> all(@PathVariable String lang, @RequestParam int startPosition,
                                     @RequestParam String targetLanguage) {

        if (lang.equalsIgnoreCase(targetLanguage)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Source and Target languages are the same.");
        }

        final int endPosition = startPosition + 5;

        List<CommonWord> commonWords = sentenceService.getCommonWords(lang, startPosition, endPosition);

        List<Sentence> sentences = sentenceService.getSentencesFromCommonWords(commonWords);

        List<String> translations = translationService.translate(lang, targetLanguage,
                sentences.stream().map(Sentence::getText).collect(Collectors.toList()));

        return sentenceService.generateGapFill(commonWords, sentences, translations);
    }

    @PostMapping("/sentences/{lang}/score")
    public void increaseScore(@PathVariable String lang, @RequestParam("sentence_ids") List<String> sentenceIds) {


    }
}
