package com.zekat.Application.controller.UserController;

import com.zekat.Application.model.User.UserDTO;
import com.zekat.Application.model.User.UserEntity;
import com.zekat.Application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO createUser(@RequestBody PostCreateUserParam parameter) {

        try {
            UserEntity user =
                    userService.createUser(parameter.getEmail(), parameter.getUsername(), parameter.getPassword());

            return UserDTO.fromEntity(user);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "We could not create this user.");
        }
    }

    @PostMapping("/login")
    public UserDTO login(@RequestBody PostLoginParam parameter) {
        try {
            String authenticationToken =
                    userService.getAuthenticationToken(parameter.getEmail(), parameter.getPassword());
            UserDTO user = UserDTO.fromEntity(userService.getUser(parameter.getEmail()));

            user.setToken(authenticationToken);

            return user;
        } catch (Exception exception) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Email or password doesn't match our records.");
        }
    }
}
