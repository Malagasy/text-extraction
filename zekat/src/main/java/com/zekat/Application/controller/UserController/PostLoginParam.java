package com.zekat.Application.controller.UserController;

import lombok.Getter;

@Getter
public class PostLoginParam {
    private String email;
    private String password;
}
