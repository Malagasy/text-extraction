package com.zekat.Application.service;

import com.zekat.Application.model.CommonWord;
import com.zekat.Application.model.GapFillResponse.GapFillResponse;
import com.zekat.Application.model.Sentence;
import com.zekat.Application.repository.CommonWordRepository;
import com.zekat.Application.repository.SentenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class SentenceService {

    @Autowired
    private SentenceRepository sentenceRepository;

    @Autowired
    private CommonWordRepository commonWordRepository;

    public List<CommonWord> getCommonWords(String lang, int startPosition, int endPosition) {
         return commonWordRepository.findByLanguageAndPositionGreaterThanEqualAndPositionLessThan(lang, startPosition, endPosition);
    }

    public List<Sentence> getSentencesFromCommonWords(List<CommonWord> commonWords) {
        List<Sentence> sentences = new ArrayList<>();

        // atm we only select the first sentence attached to the word, this needs to be changed to select sentence that user hasn't seen yet
        for (CommonWord commonWord: commonWords) {
            String sentenceId = commonWord.getSentenceIds().stream().findFirst().get();
            // should handle case where optional is failing
            Sentence sentence = sentenceRepository.findBySentenceId(sentenceId);
            sentences.add(sentence);
        }
        
        return  sentences;
    }

    public List<GapFillResponse> generateGapFill(List<CommonWord> commonWords, List<Sentence> sentences, List<String> translations) {

        List<GapFillResponse> response = new ArrayList<>();
        int index = 0;

        for(String translatedText: translations) {
            CommonWord correctAnswer = commonWords.get(index);
            Sentence sentence = sentences.get(index);

            List<CommonWord> commonWordsWithoutAnswer = commonWords.stream().filter(commonWord -> commonWord.getId() != correctAnswer.getId()).collect(Collectors.toList());

            List<String> choices = pickItemsRandomly(commonWordsWithoutAnswer, 3).stream().map(CommonWord::getWord).collect(Collectors.toList());

            GapFillResponse gapFillResponse = new GapFillResponse(sentence, translatedText, correctAnswer, choices);
            response.add(gapFillResponse);

            index++;
        }

        return response;
    }



    public static <T> List<T> pickItemsRandomly(List<T> items, int numberOfItems) {
        ArrayList<T> list = new ArrayList<>();
        ArrayList<T> cloneItems = new ArrayList<>(items);

        Random rand = new Random();

        for (int i = 0; i < numberOfItems; i++) {

            if (cloneItems.size() == 0) {
                break;
            }

            int randomIndex = rand.nextInt(cloneItems.size());

            list.add(cloneItems.remove(randomIndex));
        }

        return list;
    }
}
