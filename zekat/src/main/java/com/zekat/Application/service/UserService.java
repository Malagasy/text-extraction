package com.zekat.Application.service;

import com.zekat.Application.model.User.UserEntity;
import com.zekat.Application.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;

@Service
public class UserService {
    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12);
    @Autowired
    private UserRepository userRepository;

    public UserEntity createUser(String email, String username, String rawPassword) throws Exception {

        if (username.isEmpty()) {
            throw new Exception("Username is empty.");
        } else if (email.isEmpty() || !StringValidator.isEmail(email)) {
            throw new Exception("Email is not valid.");
        } else if (rawPassword.isEmpty()) {
            throw new Exception("Username is not set.");
        }

        if (userRepository.findByEmail(email) != null) {
            throw new Exception("User '" + email + "' already exist.");
        }


        String hashPassword = generateHashPassword(rawPassword);

        UserEntity userEntity = new UserEntity(email, hashPassword, username);

        return userRepository.save(userEntity);
    }

    public String generateHashPassword(String rawPassword) {
        return passwordEncoder.encode(rawPassword);
    }

    public String getAuthenticationToken(String email, String attemptPassword) throws Exception {

        UserEntity user = userRepository.findByEmail(email);

        if (user == null) {
            throw new Exception("Email does not match.");
        }

        boolean isMatch = passwordEncoder.matches(attemptPassword, user.getPassword());

        if (!isMatch) {
            throw new Exception("Password does not match.");
        }

        return getJWTToken(user.getId());
    }

    private String getJWTToken(Long userId) {
        SecretKey secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        String jws = Jwts.builder().setSubject(userId.toString()).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 10)).
                        signWith(secretKey).compact();

        return "Bearer " + jws;

    }

    public UserEntity getUser(String email) {
        return userRepository.findByEmail(email);
    }
}
