package com.zekat.Application.service;

import org.springframework.stereotype.Service;

@Service
public class StringValidator {
    private static final String EMAIL_REGEX = "^[\\w-_.+]*[\\w-_.]@([\\w]+\\.)+[\\w]+[\\w]$";

    public static boolean isEmail(String email) {
        return email.matches(EMAIL_REGEX);
    }
}
