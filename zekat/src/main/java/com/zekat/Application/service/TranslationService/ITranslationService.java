package com.zekat.Application.service.TranslationService;

import java.util.List;

public interface ITranslationService {
    List<String> translate(String fromLanguage, String toLanguage, List<String> texts);
}
