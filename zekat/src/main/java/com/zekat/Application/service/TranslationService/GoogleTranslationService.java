package com.zekat.Application.service.TranslationService;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

@Service("translationService")
public class GoogleTranslationService implements ITranslationService {

    private Translate service;

    public GoogleTranslationService() {
        String privateKeyGapfill = System.getenv("GCLOUD_PRIVATE_GAPFILL");
        InputStream stream = new ByteArrayInputStream(privateKeyGapfill.getBytes(StandardCharsets.UTF_8));

        try {
            ServiceAccountCredentials serviceAccountCredentials = ServiceAccountCredentials.fromStream(stream);
            this.service = TranslateOptions.newBuilder().setProjectId("text-extractor-279621")
                    .setCredentials(serviceAccountCredentials).build().getService();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> translate(String fromLanguage, String toLanguage, List<String> texts) {
        List<com.google.cloud.translate.Translation> translations = this.service.translate(texts,
                Translate.TranslateOption.sourceLanguage(fromLanguage),
                Translate.TranslateOption.targetLanguage(toLanguage),
                Translate.TranslateOption.format("text"));

        return translations.stream().map(translation -> translation.getTranslatedText()).collect(Collectors.toList());
    }
}
