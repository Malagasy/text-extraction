package com.zekat.Application.repository;

import com.zekat.Application.model.CommonWord;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommonWordRepository extends PagingAndSortingRepository<CommonWord, Long> {
    List<CommonWord> findByLanguageAndPositionGreaterThanEqualAndPositionLessThan(String language, int greaterThanEqual,
                                                                                  int lessThan);
}
