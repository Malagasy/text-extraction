package com.zekat.Application.repository;

import com.zekat.Application.model.Sentence;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface SentenceRepository extends PagingAndSortingRepository<Sentence, String> {
    List<Sentence> findByLanguage(String language);
    List<Sentence> findBySentenceIdIn(Collection<String> ids);
    Sentence findBySentenceId(String id);
}
