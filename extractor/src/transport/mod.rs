use cfg_if::cfg_if;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[derive(Debug)]
pub struct Sentence {
    pub content: String,
    pub hash: String,
    pub language: String,
}

#[derive(Debug)]
pub struct Word {
    pub id: i32,
    pub language: String,
    pub position: i32,
    pub word: String,
    pub sentence_ids: Vec<String>,
}

mod postgres {
    use mockall::mock;
    extern crate postgres as p;
    use p::{row, types, NoTls};
    use tokio_postgres::Error;

    mock! {
        pub Client {
            fn connect(params: &str, tls_mode: NoTls) -> Result<Self, Error>;
            fn execute<'a, >(&mut self, query: &str, params: &[&'a (dyn types::ToSql + Sync)]) -> Result<u64, Error>;
            fn query<'a, >(&mut self, query: &str, params: &[&'a (dyn types::ToSql + Sync)]) -> Result<Vec<row::Row>, Error>;
            fn query_one<'a, >(&mut self, query: &str, params: &[&'a (dyn types::ToSql + Sync)]) -> Result<row::Row, Error>;
        }
    }
}
cfg_if! {
    if #[cfg(test)] {
        use self::postgres::MockClient as Client;
        use ::postgres::{error, NoTls};
    }
    else {
        use ::postgres::{error, Client, NoTls};
    }
}
pub struct Transport {
    client: Client,
}

impl Transport {
    pub fn new(
        dbname: &String,
        host: &String,
        port: &String,
        user: &String,
        password: &String,
    ) -> Transport {
        Transport {
            client: Client::connect(
                format!(
                    "dbname={} host={} port={} user={} password={}",
                    dbname, host, port, user, password
                )
                .as_str(),
                NoTls,
            )
            .unwrap(),
        }
    }

    pub fn get_common_word_by_word(
        &mut self,
        language: &str,
        word: &str,
    ) -> Result<Word, error::Error> {
        let row = self
            .client
            .query_one(
                "SELECT id, language, position, word, sentence_ids FROM common_words WHERE language = $1 AND word ILIKE $2",
                &[&language, &word],
            )?;

        Ok(Word {
            id: row.get("id"),
            language: row.get("language"),
            position: row.get("position"),
            word: row.get("word"),
            sentence_ids: row.get("sentence_ids"),
        })
    }

    pub fn get_common_words(&mut self, language: &str, count: i32) -> Vec<Word> {
        let mut list: Vec<Word> = Vec::new();
        for row in self
            .client
            .query(
                "SELECT id, language, position, word, sentence_ids FROM common_words WHERE language = $1 AND position <= $2",
                &[&language, &count],
            )
            .unwrap()
        {
            list.push(Word {
                id: row.get("id"),
                language: row.get("language"),
                position: row.get("position"),
                word: row.get("word"),
                sentence_ids: row.get("sentence_ids")
            });
        }

        return list;
    }

    pub fn link_sentence_word(
        &mut self,
        sentence_hash: &str,
        word_id: i32,
    ) -> Result<bool, ::postgres::error::Error> {
        self.client.query(
           "UPDATE common_words SET sentence_ids = array_append(sentence_ids, $1::varchar) WHERE id = $2 AND $1!= all (sentence_ids)",
           &[&sentence_hash, &word_id])?;

        Ok(true)
    }
    pub fn create_object(&self, sentence: &str, language: &str) -> Sentence {
        let mut hasher = DefaultHasher::new();
        let content = String::from(sentence);
        content.hash(&mut hasher);

        Sentence {
            content,
            hash: format!("{}", hasher.finish()),
            language: String::from(language),
        }
    }

    pub fn insert(&mut self, sent_object: &Sentence) -> Result<u64, error::Error> {
        let updated_row = self.client.execute(
            "SELECT add_sentence($1, $2, $3)",
            &[
                &sent_object.hash,
                &sent_object.content,
                &sent_object.language,
            ],
        )?;

        Ok(updated_row)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn transport_new() -> Transport {
        let ctx = Client::connect_context();
        ctx.expect().returning(|_, _| Ok(Client::default()));
        let tsp = Transport::new(
            &"postgres".to_string(),
            &"0.0.0.0".to_string(),
            &"1234".to_string(),
            &"root".to_string(),
            &"pwd".to_string(),
        );

        ctx.checkpoint();

        return tsp;
    }

    #[test]
    fn transport_insert() {
        let mut tsp = transport_new();

        let sent = Sentence {
            hash: String::from("xyz"),
            content: String::from("my content"),
            language: String::from("en"),
        };

        tsp.client.expect_execute().returning(|_, _| Ok(5));

        let result = tsp.insert(&sent).unwrap();

        assert_eq!(result, 5);
    }

    #[test]
    fn transport_create_object() {
        let tsp = transport_new();

        let result = tsp.create_object("my sentence", "en");

        assert_eq!(result.content, "my sentence");
        assert_eq!(result.language, "en");
    }
}
