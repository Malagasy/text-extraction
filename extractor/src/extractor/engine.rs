pub trait ExtractorLanguage {
    //ISO 639-2
    fn get_language(&self) -> &'static str;

    fn get_stop_words(&self) -> Vec<&'static str>;
}

pub struct Sentence<'a, 'b> {
    text: &'a str,
    hash: &'b str,
}
