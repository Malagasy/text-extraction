use crate::extractor::engine::ExtractorLanguage;
pub struct English;

impl ExtractorLanguage for English {
    fn get_language(&self) -> &'static str {
        "en"
    }

    fn get_stop_words(&self) -> Vec<&'static str> {
        vec![]
    }
}
