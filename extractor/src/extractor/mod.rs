use crate::utils::strings;
use regex::Regex;
use std::boxed::Box;
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::path::Path;

mod engine;
mod langs;
use engine::ExtractorLanguage;
use langs::english::English as EnglishExtractor;

pub struct Extractor<'a> {
    engine: Box<&'a dyn ExtractorLanguage>,
    text: String,
    verbose: bool,
    target_words: Vec<String>,
}

impl<'a> Extractor<'a> {
    pub fn new(
        language: &str,
        file_path: &Path,
    ) -> Result<Extractor<'a>, Box<dyn std::error::Error>> {
        let mut extractor_map: HashMap<&str, Box<&dyn ExtractorLanguage>> = HashMap::new();
        extractor_map.insert("eng", Box::new(&EnglishExtractor {}));

        let text: String = Extractor::read_text(file_path)?;
        let engine = extractor_map.remove(language);
        if engine.is_none() {
            panic!("Language '{}' is not defined.", language);
        }

        Ok(Extractor {
            engine: engine.unwrap(),
            text,
            verbose: false,
            target_words: Vec::new(),
        })
    }

    pub fn set_target_words(&mut self, words: Vec<String>) {
        self.target_words = words;
    }

    pub fn get_target_words(&self) -> &Vec<String> {
        &self.target_words
    }
    pub fn set_verbosity(&mut self, verbose: bool) {
        self.verbose = verbose;
    }

    fn read_text(file_path: &Path) -> Result<String, std::io::Error> {
        let mut file = File::open(file_path).unwrap();
        let mut content = String::new();
        match file.read_to_string(&mut content) {
            Ok(bytes) => bytes,
            Err(e) => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("File could not be read: {:?} \n {:?}", file_path, e),
                ));
            }
        };

        Ok(strings::strip_html_tag(&content))
    }

    pub fn sentence_tokenize(&self) -> Vec<&str> {
        let raw_sentences: Vec<&str> = strings::into_sentences(&self.text);

        let re_uppercase = Regex::new(r"[A-Z]{3,}").unwrap();

        let sentences: Vec<&str> = raw_sentences
            .iter()
            .filter_map(|raw_s: &&str| {
                let enough_words: bool = strings::count_words(raw_s) > 2;
                let too_much_words: bool = strings::count_words(raw_s) > 7;
                let has_stop_words: bool = self
                    .engine
                    .get_stop_words()
                    .into_iter()
                    .any(|word: &str| raw_s.contains(word));
                let is_title: bool = raw_s.ends_with(|c: char| match c {
                    '.' | '!' | '?' => false,
                    _ => true,
                });
                let has_missing_quote: bool = raw_s
                    .matches(|c| match c {
                        // /!\ the 2 below are not the same characters
                        '‘' => true,
                        '’' => true,
                        _ => false,
                    })
                    .count()
                    == 1;
                let has_missing_bracket: bool = raw_s
                    .matches(|c| match c {
                        '(' => true,
                        ')' => true,
                        _ => false,
                    })
                    .count()
                    == 1;
                let has_common_word = raw_s.split(" ").any(|word| match word.chars().next() {
                    Some(first_letter) => match first_letter {
                        'a'..='z' => true,
                        _ => false,
                    },
                    None => false,
                });
                let has_number = raw_s.contains(|letter| match letter {
                    '0'..='9' => true,
                    _ => false,
                });

                let has_word_in_range = raw_s
                    .split(" ")
                    .any(|word| self.target_words.contains(&word.to_lowercase()));
                let start_lowercase = raw_s.starts_with(|c| match c {
                    'a'..='z' => true,
                    _ => false,
                });

                let too_many_uppercase = re_uppercase.is_match(raw_s);

                if too_many_uppercase
                    || start_lowercase
                    || !has_common_word
                    || !has_word_in_range
                    || has_number
                    || has_missing_bracket
                    || has_missing_quote
                    || !enough_words
                    || too_much_words
                    || has_stop_words
                    || is_title
                {
                    return None;
                }

                Some(raw_s.trim())
            })
            .collect();

        return sentences;
    }
}
