pub mod strings {
    extern crate regex;
    use regex::Regex;

    pub fn strip_html_tag(text: &str) -> String {
        let re = Regex::new(r"<.+?>").unwrap();
        let stripped = re.replace_all(text, "");
        stripped.to_string()
    }

    pub fn count_words(sentence: &str) -> usize {
        let re = Regex::new(r"[[:alnum:]]+").unwrap();
        let splitted: Vec<&str> = sentence
            .split(" ")
            .filter(|item| re.is_match(item))
            .collect();
        splitted.len()
    }

    pub fn into_sentences<'a>(text: &'a String) -> Vec<&'a str> {
        let r = Regex::new(r".*?(([^A-Z]+\.)|([!?] )|\n)").unwrap();
        let mut capture_locations = r.capture_locations();
        let mut position = 0;
        let mut raw_sentences: Vec<&'a str> = Vec::new();

        while position != text.len() {
            let option = r.captures_read_at(&mut capture_locations, text, position);

            if option.is_none() {
                break;
            }
            let matches = option.unwrap();

            let text = matches.as_str();
            position = matches.end();
            raw_sentences.push(text.trim());
        }

        raw_sentences
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn count_words_pass() {
            assert_eq!(count_words("3"), 1);
            assert_eq!(count_words(""), 0);
            assert_eq!(count_words("This is a sentence "), 4);
            assert_eq!(count_words("This is a very long sentence."), 6);
            assert_eq!(count_words("Does this, handle, commas?"), 4);
            assert_eq!(count_words("I play football ."), 3);
        }

        #[test]
        fn into_sentences_pass() {
            let text: String = String::from(
                "This is the first sentence ! Mr T. Jorge is here. This one will be splitted one. She said angrily 'give me my keys!'. But not this one",
            );
            let sentences: Vec<&str> = into_sentences(&text);
            println!("{:?}", sentences);
            assert!(sentences.contains(&"This is the first sentence !"));
            assert!(sentences.contains(&"This one will be splitted one."));
            assert!(sentences.contains(&"Mr T. Jorge is here."));
            assert!(!sentences.contains(&"But not this one"));
            assert!(sentences.contains(&"She said angrily 'give me my keys!'."));
        }
    }
}
