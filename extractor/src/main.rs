extern crate clap;
extern crate glob;
extern crate regex;
use clap::{App, Arg, ArgMatches};
use glob::glob;
use std::env;
use std::path::{Path, PathBuf};
mod extractor;
mod transport;
mod utils;

fn main() {
    let matches: ArgMatches = App::new("Content Extractor")
        .version("1.0")
        .author("Andy R <andyralanto@gmail.com>")
        .args(&[
            Arg::with_name("file_path")
                .short("f")
                .long("file")
                .help("ASCII file to open")
                .takes_value(true)
                .required(true),
            Arg::with_name("language")
                .short("l")
                .long("lang")
                .help("Language of the file to parse")
                .takes_value(true)
                .required(true),
            Arg::with_name("target_words")
                .short("t")
                .long("target-words")
                .help("Pick sentences containing the most common $target_words(number) words.")
                .takes_value(true)
                .required(true),
            Arg::with_name("print")
                .short("p")
                .long("print")
                .help("Print sentences but won't add to database.")
                .takes_value(false),
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("increase app verbosity")
                .takes_value(false),
        ])
        .get_matches();

    let glob_path: &str = matches.value_of("file_path").unwrap();
    let lang: &str = matches.value_of("language").unwrap();
    let count_target_words: i32 = matches
        .value_of("target_words")
        .unwrap()
        .parse::<i32>()
        .unwrap();
    let verbose: bool = match matches.occurrences_of("verbose") {
        0 => false,
        _ => true,
    };
    let print: bool = match matches.occurrences_of("print") {
        0 => false,
        _ => true,
    };

    let tsp_dbname = env::var("DB_NAME").expect("'DB_NAME' env var is missing.");
    let tsp_host = env::var("DB_HOST").expect("'DB_HOST' env var is missing.");
    let tsp_port = env::var("DB_PORT").expect("'DB_PORT' env var is missing.");
    let tsp_user = env::var("DB_USER").expect("'DB_USER' env var is missing.");
    let tsp_password = env::var("DB_PASSWORD").expect("'DB_PASSWORD' env var is missing.");

    let mut inserted_rows = 0;
    let mut total_sentences = 0;
    for entry in glob(glob_path).expect("Failed to read glob pattern") {
        let file: PathBuf = entry.expect("Failed to read file");

        let mut transport =
            transport::Transport::new(&tsp_dbname, &tsp_host, &tsp_port, &tsp_user, &tsp_password);
        let common_words: Vec<transport::Word> =
            transport.get_common_words(lang, count_target_words);

        let mut instance = match extractor::Extractor::new(lang, file.as_path()) {
            Ok(instance) => instance,
            Err(e) => {
                println!(
                    "/!\\ \n This file cannot be read and will be ignore \n /!\\ {:?}",
                    e
                );
                continue;
            }
        };
        instance.set_verbosity(verbose);
        instance.set_target_words(
            common_words
                .into_iter()
                .map(|word_struct| word_struct.word.to_lowercase())
                .collect(),
        );

        let sentences: Vec<&str> = instance.sentence_tokenize();
        for sentence in &sentences {
            total_sentences += 1;
            for word in sentence.split(" ") {
                let word_lower: String = word.to_lowercase();

                if !instance.get_target_words().contains(&word_lower) {
                    continue;
                }

                let common_word: transport::Word =
                    match transport.get_common_word_by_word(&lang, &word_lower) {
                        Ok(word) => word,
                        Err(e) => {
                            panic!(
                            "Common word not found in database. language '{}' word '{}' \n {:?}",
                            lang, word_lower, e
                        );
                        }
                    };

                if common_word.sentence_ids.len() == 10 {
                    continue;
                }

                if print {
                    println!("sentence={}", sentence);
                    break;
                }

                let sent_object = transport.create_object(sentence, lang);

                let row = match transport.insert(&sent_object) {
                    Ok(row) => {
                        if verbose {
                            println!("Sentence inserted: {:?}", sent_object);
                        }
                        row
                    }
                    Err(e) => {
                        panic!("Could not insert the sentence. {:?} {:?}", sent_object, e);
                    }
                };

                match transport.link_sentence_word(&sent_object.hash, common_word.id) {
                    Ok(_) => true,
                    Err(e) => {
                        panic!("sentence not linked to common word. {:?}", e);
                    }
                };

                inserted_rows += row;
                break;
            }
        }
    }
    println!(
        "inserted/total sentences: {}/{}",
        inserted_rows, total_sentences
    );
}
