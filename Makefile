run-db-local:
	docker-compose -f database/docker-compose.yml run --name database --service-ports -d --rm database postgres -c 'listen_addresses=*'

run-flyway:
	docker-compose -f database/docker-compose.yml build --no-cache flywaydb
	docker-compose -f database/docker-compose.yml run --rm --use-aliases flywaydb migrate

run-flyway-dev:
	docker-compose -f database/docker-compose.yml run \
		-e GCLOUD_SERVICE_KEY='$(GCLOUD_SERVICE_KEY)' \
		--rm --use-aliases --entrypoint /bin/bash flywaydb -c \
		'echo $$GCLOUD_SERVICE_KEY > json_creds; \
       	       ./cloud_sql_proxy -instances=$(GCLOUD_DB_INSTANCE_CONNEXION)=tcp:$(DB_PORT) \
	       	-credential_file=./json_creds & flyway migrate; \
		rm json_creds'

login-db-local:
	PGPASSWORD=$(DB_PASSWORD) psql -d $(DB_NAME) -h 0.0.0.0 -U $(DB_USER)
