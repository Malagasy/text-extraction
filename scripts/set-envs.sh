#!/bin/bash


if [[ "${1}" == "false" ]]
then
	echo "Program will set default values defined in scripts"
	export DB_NAME=postgres
       	export DB_USER=postgres 
	export DB_PASSWORD=dummypwd
	export DB_PORT=5432
	export DB_HOST=database
else
	# envs EXTRACTOR_DATABASE_* are set within CI environments
	echo "Program will pull EXTRACTOR_DATABASE_* keys from environments"
	export DB_NAME=$EXTRACTOR_DATABASE_NAME
	export DB_USER=$EXTRACTOR_DATABASE_USER
	export DB_PASSWORD=$EXTRACTOR_DATABASE_PASSWORD
	export DB_HOST=$EXTRACTOR_DATABASE_HOST
	export DB_PORT=$EXTRACTOR_DATABASE_PORT
fi	
