#!/bin/bash

script_path="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
project_path=$( realpath $script_path/.. )
folder=$project_path/$1
branch_name=$( git rev-parse --abbrev-ref HEAD )

echo -e "[branch/$branch_name] Check if there are changes in folder $folder"

if [[ $branch_name == "master" ]]
then
	git diff --quiet HEAD HEAD^ -- $folder	
else
	git diff --quiet HEAD origin/master -- $folder
fi	

if [[ $? -eq 1 ]]
then
	echo -e "Changes detected!"
	exit 0 
fi
		
echo -e "No changes detected."
exit 1
