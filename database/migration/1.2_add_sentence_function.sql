CREATE OR REPLACE FUNCTION add_sentence(
	sentence_id text,
	sentence text,
	language text)
RETURNS boolean as $$
BEGIN
	INSERT INTO sentences (sentence_id, text, language)
		VALUES (sentence_id, sentence, language);

	RETURN FOUND;
END; $$

LANGUAGE plpgsql;
